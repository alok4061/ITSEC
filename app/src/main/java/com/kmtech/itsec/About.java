package com.kmtech.itsec;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.kmtech.itsec.about_fragments.AboutInstitute;
import com.kmtech.itsec.about_fragments.DirectorMsg;
import com.kmtech.itsec.about_fragments.GoverningBoard;
import com.kmtech.itsec.about_fragments.OurVision;

import java.util.ArrayList;
import java.util.List;

public class About extends AppCompatActivity {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        toolbar = (Toolbar) findViewById(R.id.about_toolbar);
        setSupportActionBar(toolbar);

        //ACTION BAR
        ActionBar actionBar = getSupportActionBar();
        if(getSupportActionBar()!= null){
            //actionBar.setIcon(R.drawable.ic_about);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("About Us");
        }

        viewPager = (ViewPager) findViewById(R.id.about_tab_viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.about_tabs);
        tabLayout.setupWithViewPager(viewPager);


    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new AboutInstitute(), "About Institute");
        adapter.addFrag(new OurVision(), "Our Vision");
        adapter.addFrag(new DirectorMsg(), "Director's Message");
        adapter.addFrag(new GoverningBoard(), "Governing Boards");

        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
