package com.kmtech.itsec.utility;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kmtech.itsec.R;

import java.util.List;

/**
 * Created by Ankit Mishra on 5/18/2016.
 */
public class AntiAdapter extends RecyclerView.Adapter<AntiAdapter.MyViewHolder> {
    private List<Item> itemList;
    private Context myContext;


    public AntiAdapter(Context c){
        myContext = c;

    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView name, desig, email, phone;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.tVArcName);
            desig=(TextView) view.findViewById(R.id.tVArcDes);
            email=(TextView) view.findViewById(R.id.tVArcEmail);
            phone=(TextView) view.findViewById(R.id.tVArcPhone);
        }
    }
    public AntiAdapter(List<Item> itemList) {
        this.itemList = itemList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.arc_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Item item = itemList.get(position);
        holder.name.setText(item.getName());
        holder.desig.setText(item.getDesignation());
        holder.email.setText(item.getEmail());
        holder.phone.setText(item.getPhone());
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }
}

