package com.kmtech.itsec.utility;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kmtech.itsec.R;

import java.util.List;

/**
 * Created by Ankit Mishra on 5/2/2016.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder>  {
    private List<Item> itemList;
    private Context myContext;


    public RecyclerAdapter(Context c){
        myContext = c;

    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView  name, desig;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.tVDirName);
            desig=(TextView) view.findViewById(R.id.tVDirDes);
        }
    }
    public RecyclerAdapter(List<Item> itemList) {
        this.itemList = itemList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Item item = itemList.get(position);
        holder.name.setText(item.getName());
        holder.desig.setText(item.getDesignation());
            }

    @Override
    public int getItemCount() {
        return itemList.size();
    }
}
