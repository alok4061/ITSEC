package com.kmtech.itsec;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kmtech.itsec.utility.ConnectionUtility;

import java.util.Timer;
import java.util.TimerTask;

public class Splash extends AppCompatActivity {

    private Activity activity = Splash.this;
    ProgressDialog pdial;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Timer timer = new Timer();
        timer.schedule(new splash(), 3000);

    }

    class splash extends TimerTask {

        @Override
        public void run() {
            Intent intent = new Intent(Splash.this,MainActivity.class);
            activity.finish();
            startActivity(intent);
        }

    }
    }