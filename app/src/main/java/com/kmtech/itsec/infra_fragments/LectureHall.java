package com.kmtech.itsec.infra_fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kmtech.itsec.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LectureHall.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LectureHall#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LectureHall extends Fragment {
    public LectureHall() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_lecture_hall, container, false);
    }

}