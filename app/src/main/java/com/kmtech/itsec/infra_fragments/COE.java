package com.kmtech.itsec.infra_fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kmtech.itsec.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link COE.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link COE#newInstance} factory method to
 * create an instance of this fragment.
 */
public class COE extends Fragment {
    public COE() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_coe, container, false);
    }

}