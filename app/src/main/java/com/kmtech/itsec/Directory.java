package com.kmtech.itsec;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.kmtech.itsec.utility.DividerItemDecoration;
import com.kmtech.itsec.utility.Item;
import com.kmtech.itsec.utility.RecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

public class Directory extends AppCompatActivity {
    private Activity activity = Directory.this;
    private RecyclerView recyclerView;
    RecyclerAdapter eAdapter;

    private List<Item> itemList = new ArrayList<Item>();
    private RecyclerView.Adapter rAdapter;
    private RecyclerView.LayoutManager mlayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_directory);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //RecylerView
        recyclerView = (RecyclerView) findViewById(R.id.rV);
        eAdapter = new RecyclerAdapter(itemList);

        //ACTION BAR
        ActionBar actionBar = getSupportActionBar();
        if(getSupportActionBar()!= null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Directory");
            recyclerView.setHasFixedSize(true);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(eAdapter);

            prepareDirectoryData();
        }
    }
    public void prepareDirectoryData() {
        Item item = new Item("Dr.Vineet Kansal","Director", "dir.engg@its.edu.in","01202331010");
        itemList.add(item);
        item = new Item("Dr.Gagandeep Arora","Dean- Academic Administration","dean.acada.engg@its.edu.in","01202331017");
        itemList.add(item);
        item = new Item("Dr.Rakesh Dubey","Head- Applied Sciences & Humanities","hod.ash@its.edu.in","01202331026");
        itemList.add(item);
         item = new Item("Dr.Ashish Kumar","Head-Computer Science & Engineering", "hod.cse@its.edu.in","01202331023");
        itemList.add(item);
        item = new Item("Dr.Vinay Kakkar","Head- Electrical & Electronics Engineering", "hod.eee@its.edu.in","01202331025");
        itemList.add(item);
        item = new Item("Dr.Sanjay Yadav","DSW & Head- Mechanical Engineering","dean.sw.engg@its.edu.in","01202331024");
        itemList.add(item);
        item = new Item("Prof.Ankur Srivastava","Head-MBA","hod.mba.gn@its.edu.in","9582647616");
        itemList.add(item);
        item = new Item("Dr.Dinesh Chandra","Head- Electronics & Communications Engineering","hod.ece@its.edu.in","01202331038");
        itemList.add(item);
        item = new Item("Prof.Jitendra Anand","Head- Civil Engineering","hod.civil@its.edu.in","08510010839");
        itemList.add(item);
        item = new Item("Mr.Kishan Singh","Administrator","admin.engg@its.edu.in","08506890033");
        itemList.add(item);
        item = new Item("Mr.Roshan Ali","Personnel Officer","hr.engg@its.edu.in","01202331020");
        itemList.add(item);
        eAdapter.notifyDataSetChanged();
    }
}
