package com.kmtech.itsec;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.kmtech.itsec.about_fragments.AboutInstitute;
import com.kmtech.itsec.about_fragments.DirectorMsg;
import com.kmtech.itsec.about_fragments.GoverningBoard;
import com.kmtech.itsec.about_fragments.OurVision;
import com.kmtech.itsec.placement_fragments.AboutCRC;
import com.kmtech.itsec.placement_fragments.CrcTeam;
import com.kmtech.itsec.placement_fragments.MajorRecruiter;
import com.kmtech.itsec.placement_fragments.PlacementAchievement;
import com.kmtech.itsec.placement_fragments.Training;

import java.util.ArrayList;
import java.util.List;

public class Placements extends AppCompatActivity {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_placements);
        toolbar = (Toolbar) findViewById(R.id.placement_toolbar);
        setSupportActionBar(toolbar);
        //ACTION BAR
        ActionBar actionBar = getSupportActionBar();
        if(getSupportActionBar()!= null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Placement");
        }

        viewPager = (ViewPager) findViewById(R.id.placement_tab_viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.placement_tabs);
        tabLayout.setupWithViewPager(viewPager);


    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new AboutCRC(), "About CRC");
        adapter.addFrag(new MajorRecruiter(), "Major Recruiters");
        adapter.addFrag(new PlacementAchievement(), "Placement Achievement");
        adapter.addFrag(new Training(), "Training");
        adapter.addFrag(new CrcTeam(), "CRC Team");

        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
