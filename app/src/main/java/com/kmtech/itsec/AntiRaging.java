package com.kmtech.itsec;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.kmtech.itsec.utility.AntiAdapter;
import com.kmtech.itsec.utility.DividerItemDecoration;
import com.kmtech.itsec.utility.Item;
import com.kmtech.itsec.utility.RecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

public class AntiRaging extends AppCompatActivity {
    private Activity activity = AntiRaging.this;
    private RecyclerView recyclerView;
    AntiAdapter eAdapter;

    private List<Item> itemList = new ArrayList<Item>();
    private RecyclerView.Adapter rAdapter;
    private RecyclerView.LayoutManager mlayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anti_raging);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //RecylerView
        recyclerView = (RecyclerView) findViewById(R.id.rVArc);
        eAdapter = new AntiAdapter(itemList);
        //ACTION BAR
        ActionBar actionBar = getSupportActionBar();
        if (getSupportActionBar() != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Anti-Ragging Cell");
            recyclerView.setHasFixedSize(true);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(eAdapter);

            prepareAntiData();


        }
    }

    private void prepareAntiData() {
        Item item = new Item("Dr. Sanjay Yadav","Chairman, ARC&DSW ", "dean.sw.engg@its.edu.in","09582647615");
        itemList.add(item);
        item = new Item("Dr.Gagandeep Arora","Member, ARC&Dean(AA)","dean.acada.engg@its.edu.in","07838555875");
        itemList.add(item);
        item = new Item("Dr.Ashish Kumar","Member, ARC&Head (CSE)","hod.cse@its.edu.in","09540987274");
        itemList.add(item);
        item = new Item("Dr.Rashmi Gupta","Member, ARC Professor (ASH)", "rashmigupta@its.edu.in","07838555876");
        itemList.add(item);
        item = new Item("Mr.Kishan Singh","Administrator&Member, ARC", "admin.engg@its.edu.in","08506890033");
        itemList.add(item);
        eAdapter.notifyDataSetChanged();
    }
}
