package com.kmtech.itsec;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class Research extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_research);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //ACTION BAR
        ActionBar actionBar = getSupportActionBar();
        if(getSupportActionBar()!= null){
            //actionBar.setIcon(R.drawable.ic_about);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Research & Publications");
        }
    }

}
