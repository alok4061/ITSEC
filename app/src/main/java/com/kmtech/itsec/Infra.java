package com.kmtech.itsec;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import com.kmtech.itsec.infra_fragments.AppliedLab;
import com.kmtech.itsec.infra_fragments.Auditorium;
import com.kmtech.itsec.infra_fragments.CELab;
import com.kmtech.itsec.infra_fragments.COE;
import com.kmtech.itsec.infra_fragments.Cafeteria;
import com.kmtech.itsec.infra_fragments.ComputerLabs;
import com.kmtech.itsec.infra_fragments.ECELab;
import com.kmtech.itsec.infra_fragments.EEELab;
import com.kmtech.itsec.infra_fragments.Hostel;
import com.kmtech.itsec.infra_fragments.LectureHall;
import com.kmtech.itsec.infra_fragments.Library;
import com.kmtech.itsec.infra_fragments.MELab;
import com.kmtech.itsec.infra_fragments.MedicalFaculty;
import com.kmtech.itsec.infra_fragments.Transport;

import java.util.ArrayList;
import java.util.List;

public class Infra extends AppCompatActivity {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_infra);
        toolbar = (Toolbar) findViewById(R.id.infra_toolbar);
        setSupportActionBar(toolbar);

        //ACTION BAR
        ActionBar actionBar = getSupportActionBar();
        if(getSupportActionBar()!= null){
            //actionBar.setIcon(R.drawable.ic_about);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Infrastructure");
        }

        viewPager = (ViewPager) findViewById(R.id.infra_tab_viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.infra_tabs);
        tabLayout.setupWithViewPager(viewPager);


    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new ComputerLabs(), "Computer Lab");
        adapter.addFrag(new Library(), "Library");
        adapter.addFrag(new Hostel(), "Hostel");
        adapter.addFrag(new Cafeteria(), "Cafeteria");
        adapter.addFrag(new Transport(), "Transport");
        adapter.addFrag(new LectureHall(), "Lecture Hall");
        adapter.addFrag(new Auditorium(), "Auditorium");
        adapter.addFrag(new MedicalFaculty(), "Medical Facilty");
        adapter.addFrag(new ECELab(), "ECE Labs");
        adapter.addFrag(new EEELab(), "EEE Labs");
        adapter.addFrag(new MELab(), "ME Labs");
        adapter.addFrag(new CELab(), "Civil Labs");
        adapter.addFrag(new AppliedLab(), "Applied Labs");
        adapter.addFrag(new COE(), "Centre of Exellence");


        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
