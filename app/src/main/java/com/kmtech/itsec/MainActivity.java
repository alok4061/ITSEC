package com.kmtech.itsec;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import com.viewpagerindicator.CirclePageIndicator;
import com.kmtech.itsec.utility.ConnectionUtility;
import com.kmtech.itsec.utility.ImageAdapter;
import android.os.Handler;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private Activity activity = MainActivity.this;
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private static final Integer[] IMAGES= {R.drawable.a,R.drawable.b,R.drawable.c,R.drawable.d,R.drawable.e,R.drawable.f,R.drawable.g};
    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();
    Button antiRag,enewsEce,enewsCse,enewsAdm, downloads,enewsEee,enewsMba,enewsIts,aluniReg,mechInfo,fdp,fdpIa,ica;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        init();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ConnectionUtility.checkConnection(activity)) {
                    showMapActivity();

                } else {

                    showToast("No Internet Connection");
                }
            }
        });
                antiRag=(Button)findViewById(R.id.btnArcMain);
                enewsEce=(Button)findViewById(R.id.btnEEceMain);
                 enewsCse=(Button)findViewById(R.id.btnECseMain);
                 enewsAdm= (Button)findViewById(R.id.btnAdmMain);
                         downloads=(Button)findViewById(R.id.btnDownMain);
                 enewsEee=(Button)findViewById(R.id.btnEEeeMain);
                 enewsMba=(Button)findViewById(R.id.btnEMbaMain);
                 enewsIts=(Button)findViewById(R.id.btnEItsMain);
                 aluniReg=(Button)findViewById(R.id.btnARegMain);
                 mechInfo=(Button)findViewById(R.id.btnMIMain);
                 fdp=(Button)findViewById(R.id.btnFdpMain);
                 fdpIa=(Button)findViewById(R.id.btnFIaMain);
        ica=(Button)findViewById(R.id.btnEItsMain);
        antiRag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent j = new Intent(getApplicationContext(),AntiRaging.class);

                startActivity(j);

            }
        });
        enewsEce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent w =new Intent(Intent.ACTION_VIEW);
                w.setData(Uri.parse("http://itsecgn.edu.in/electronika/ELECTRONIKAMAINPAGE.htm"));
                startActivity(w);
            }
        });
        enewsCse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent w =new Intent(Intent.ACTION_VIEW);
                w.setData(Uri.parse("http://itsecgn.edu.in/cse/pages/home.htm"));
                startActivity(w);

            }
        });
        enewsAdm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent w =new Intent(Intent.ACTION_VIEW);
                w.setData(Uri.parse("http://www.itsecgn.edu.in/Home/Admission-Procedures.aspx"));
                startActivity(w);
            }
        });
        enewsEee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent w =new Intent(Intent.ACTION_VIEW);
                w.setData(Uri.parse("http://www.itsecgn.edu.in/EEE-TIMES/EEE%20TIMES%20-%20VOL.%203.html"));
                startActivity(w);

            }
        });
        enewsMba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent w =new Intent(Intent.ACTION_VIEW);
                w.setData(Uri.parse("http://www.itsecgn.edu.in/MBAnewsletter/Pages/home.html"));
                startActivity(w);
            }
        });
        enewsIts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent w =new Intent(Intent.ACTION_VIEW);
                w.setData(Uri.parse("http://www.itsecgn.edu.in/enewsletterdec15/newsletter.html"));
                startActivity(w);
            }
        });
        aluniReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent w =new Intent(Intent.ACTION_VIEW);
                w.setData(Uri.parse("https://docs.google.com/a/its.edu.in/forms/d/1y5_mmbLaDbXdDH2OWExc2HTSgPew6jk_ngX-6kVd32M/viewform"));
                startActivity(w);

            }
        });
        mechInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent w =new Intent(Intent.ACTION_VIEW);
                w.setData(Uri.parse("http://www.itsecgn.edu.in/mechinfo/mechinfo.html"));
                startActivity(w);
            }
        });
        fdp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent w =new Intent(Intent.ACTION_VIEW);
                w.setData(Uri.parse("http://www.itsecgn.edu.in/FDP2016/FDP2016.html"));
                startActivity(w);
            }
        });
        fdpIa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent w =new Intent(Intent.ACTION_VIEW);
                w.setData(Uri.parse("http://www.itsecgn.edu.in/FDP-IAPPSCE/brochure.html"));
                startActivity(w);
            }
        });
        ica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent w =new Intent(Intent.ACTION_VIEW);
                w.setData(Uri.parse("http://www.itsecgn.edu.in/ICAEDC2016/Icaedc.html"));
                startActivity(w);
            }
        });
        downloads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent w =new Intent(Intent.ACTION_VIEW);
                w.setData(Uri.parse("http://www.itsecgn.edu.in/downloads/Antiragging.aspx"));
                startActivity(w);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }
    public void showToast(String msg) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);

        alertDialogBuilder.setMessage(msg)
                .setCancelable(false)
                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        activity.recreate();
                        dialog.cancel();
                    }
                });

        alertDialogBuilder.show();
    }
    private void showMapActivity(){
        Intent intent = new Intent(MainActivity.this,MapsActivity.class);
        activity.finish();
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            DirectoryOpen();
            return true;
        }
        if (id == R.id.action_web) {

            webOpen();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void webOpen() {

        Intent w =new Intent(Intent.ACTION_VIEW);
        w.setData(Uri.parse("http://www.itsecgn.edu.in/"));
        startActivity(w);

    }

    private void DirectoryOpen() {

        Intent dir =new Intent(getApplicationContext(),Directory.class);
        startActivity(dir);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_about) {
            About();
            return  true;

        }   else if (id == R.id.nav_infra) {
            Infra();return  true;

        } else if (id == R.id.nav_placement) {
            Placement();
            return true;
        }else if (id == R.id.nav_research) {

            ResearchPublications();
            return  true;

        }
        else if (id == R.id.nav_call) {
            Call();

        }else if (id == R.id.nav_email) {
            if (ConnectionUtility.checkConnection(activity)) {
                Email();

            } else {

                showToast("No Internet Connection");
            }

        }else if (id == R.id.nav_fb) {

            if (ConnectionUtility.checkConnection(activity)) {
                Facebook();

            } else {

                showToast("No Internet Connection");
            }

        }else if (id == R.id.nav_youtube) {
            if (ConnectionUtility.checkConnection(activity)) {
                Youtube();

            } else {

                showToast("No Internet Connection");
            }

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void ResearchPublications() {
        Intent research = new Intent(getApplicationContext(),Research.class);
        startActivity(research);
    }

    private void Infra() {
        Intent infra = new Intent(getApplicationContext(),Infra.class);
        startActivity(infra);
    }
    private  void Placement(){
        Intent plac = new Intent(getApplicationContext(),Placements.class);
        startActivity(plac);

    }

    private void Email() {
        String to = "hr.engg@its.edu.in";
        String cc ="dir.engg@its.edu.in";
        Intent e = new Intent(Intent.ACTION_SEND);
        e.putExtra(Intent.EXTRA_EMAIL, new String[]{to});
        e.putExtra(Intent.EXTRA_CC,new String[]{cc});
        e.setType("message/rfc822");
        startActivity(e);
    }

    private void Youtube() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("https://www.youtube.com/user/itsengg"));
        startActivity(intent);
    }

    private void Call() {
        Intent c = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:0120-2331000"));
        startActivity(c);
    }

    private void About() {
        Intent i = new Intent(getApplicationContext(),About.class);
        startActivity(i);

    }
    private void Facebook(){Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
        String facebookUrl = getFacebookPageURL(this);
        facebookIntent.setData(Uri.parse(facebookUrl));
    }


//method to get the right URL to use in the intent
        public String getFacebookPageURL(Context context) {
            String facebookUrl = "https://www.facebook.com/ITSEC.Greater.Noida";
            String facebookID = "ITSEC.Greater.Noida";

            PackageManager packageManager = context.getPackageManager();
            try {
                int versionCode = getApplicationContext().getPackageManager().getPackageInfo("com.facebook.katana", 0).versionCode;

                if(!facebookID.isEmpty()) {
                    // open the Facebook app using facebookID (fb://profile/facebookID or fb://page/facebookID)
                    Uri uri = Uri.parse("fb://page/" + facebookID);
                    startActivity(new Intent(Intent.ACTION_VIEW, uri));
                } else if (versionCode >= 3002850 && !facebookUrl.isEmpty()) {
                    // open Facebook app using facebook url
                    Uri uri = Uri.parse("fb://facewebmodal/f?href=" + facebookUrl);
                    startActivity(new Intent(Intent.ACTION_VIEW, uri));
                } else {
                    // Facebook is not installed. Open the browser
                    Uri uri = Uri.parse(facebookUrl);
                    startActivity(new Intent(Intent.ACTION_VIEW, uri));
                }
            } catch (PackageManager.NameNotFoundException e) {
                // Facebook is not installed. Open the browser
                Uri uri = Uri.parse(facebookUrl);
                startActivity(new Intent(Intent.ACTION_VIEW, uri));
            }
            return facebookUrl; //normal web url
        }

    //SLIDING IMAGES
    private void init() {


        for(int i=0;i<IMAGES.length;i++)
            ImagesArray.add(IMAGES[i]);

        mPager = (ViewPager) findViewById(R.id.pager);


        mPager.setAdapter(new ImageAdapter(MainActivity.this,ImagesArray));


        CirclePageIndicator indicator = (CirclePageIndicator)
                findViewById(R.id.indicator);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

        indicator.setRadius(5 * density);



        NUM_PAGES =IMAGES.length;



        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 5000, 5000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }
}
